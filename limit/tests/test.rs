#[cfg(test)]
mod tests {
    use limit::limit;
    use core::f64::INFINITY;


    pub fn simple_function_one(x: f64) -> f64 {
        x / x
    }
    pub fn simple_function_two(x: f64) -> f64 {
        x / x.powf(2.0)
    }
    pub fn simple_function_three(x: f64) -> f64 {
        x / x.powf(3.0)
    }
    pub fn simple_function_four(x: f64) -> f64 {
        1.0 / x
    }
    pub fn round_result(result: f64) -> f64{
        (result * 100.0).round() / 100.0
    }


    #[test]
    fn test_sf1_1() {
        assert_eq!(round_result(limit(2.0, &simple_function_one)), 1.0);
    }
    #[test]
    fn test_sf1_2() {
        assert_eq!(round_result(limit(5.0, &simple_function_one)), 1.0);
    }
    #[test]
    fn test_sf1_zerotest() {
        assert!(limit(0.0, &simple_function_one).is_nan());
    }
    #[test]
    fn test_sf2_1() {
        assert_eq!(round_result(limit(2.0, &simple_function_two)), 0.5)
    }
    #[test]
    fn test_sf2_zerotest(){
        assert!(limit(0.0, &simple_function_two).is_nan());
    }
    #[test]
    fn test_sf2_inftest(){
        assert!(limit(INFINITY, &simple_function_two).is_nan());
    }
    #[test]
    fn test_sf3_inftest(){
        assert!(limit(INFINITY, &simple_function_three).is_nan());
    }
    #[test]
    fn test_sf4_inftest(){
        assert_eq!(limit(INFINITY, &simple_function_four), 0.0);
    }

}
