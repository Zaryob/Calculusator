
#[cfg(test)]
mod simple_otd_tests {
    // one time derivatives
    use derivative::derivative;

    pub fn simple_function_one(x: f64) -> f64 {
        x.powf(2.0)+x+1.0
    }
    pub fn simple_function_two(x: f64) -> f64 {
        x.powf(3.0)+ 3.0 * x.powf(2.0)+ 3.0 * x  + 1.0
    }
    pub fn round_result(result: f64) -> f64{
        (result * 100.0).round() / 100.0
    }


    #[test]
    fn test_sf1_1() {
        assert_eq!(round_result(derivative(2.0, &simple_function_one)), 5.0);
    }

    #[test]
    fn test_sf1_2() {
        assert_eq!(round_result(derivative(5.0, &simple_function_one)), 11.0);
    }
    #[test]
    fn test_sf1_zerotest() {
        assert_eq!(round_result(derivative(0.0, &simple_function_one)), 1.0);
    }

    #[test]
    fn test_sf2_1() {
        assert_eq!(round_result(derivative(2.0, &simple_function_two)), 27.0);
    }
    #[test]
    fn test_sf2_2() {
        assert_eq!(round_result(derivative(5.0, &simple_function_two)), 108.0);
    }
    #[test]
    fn test_sf2_zerotest() {
        assert_eq!(round_result(derivative(0.0, &simple_function_two)), 3.0);
    }

    /*
    #[test]
    fn test_sf1() {
        assert_eq!(round_result(crate::derivative(2.0, &simple_function_one)), 5.0);
    }
    #[test]
    fn test_sf1() {
        assert_eq!(round_result(crate::derivative(2.0, &simple_function_one)), 5.0);
    }
    */
}
