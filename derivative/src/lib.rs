pub fn derivative(value: f64, function: &dyn Fn(f64) -> f64) -> f64 {
    let h: f64 = 0.00000000001;
    let top = function(value + h) - function(value);
    let bottom = h;
    let slope = top / bottom;
    slope
}
